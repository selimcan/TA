#!/usr/bin/bash

# Feature: Given a text file with one tree representation per line,
#          read trees from the file and
#          output the height for each input tree (one height per line).


  if diff <(./1_heights wsj_0100.tree) acceptance_tests/pg01/1_heights/expected_heights.txt
  then
    echo "PASSED: $0"
    exit 0
  else
    echo "FAILED: $0"
    exit 1
  fi
