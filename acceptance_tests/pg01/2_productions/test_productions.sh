#!/usr/bin/bash

# Feature: Given a text file with one tree representation per line,
#          read trees from the file and
#          output *all* (i.e. both syntactic and lexical) LTG productions
#          necessary to generate the input trees contained in the file.


all_scenarios_passed=true

# Scenario 1:
# Traverse trees in breadth first manner
  if diff <(./2_productions_bft wsj_0100.tree|sort) <(sort acceptance_tests/pg01/2_productions/productions.all)
  then
    echo "     PASSED: Scenario 1 of $0"
  else
    echo "     FAILED: Scenario 1 of $0"
    all_scenarios_passed=false
  fi

# Scenario 2:
# Traverse trees in depth first (pre-order) manner
  if diff <(./2_productions_dft wsj_0100.tree) acceptance_tests/pg01/2_productions/productions.all
  then
    echo "     PASSED: Scenario 2 of $0"
  else
    echo "     FAILED: Scenario 2 of $0"
    all_scenarios_passed=false
  fi

# Scenario 3:
# Generate only unique productions (in the same order as in Scenario 2 (depth-first pre-order))
  if diff <(./2\*_productions_unique wsj_0100.tree) acceptance_tests/pg01/2_productions/productions.once
  then
    echo "     PASSED: Scenario 3 of $0"
  else
    echo "     FAILED: Scenario 3 of $0"
    all_scenarios_passed=false
  fi

if [ "$all_scenarios_passed" = true ]; then
    echo "PASSED: $0"
    exit 0
else
    echo "FAILED: $0"
    exit 1
fi
