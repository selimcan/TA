#!/usr/bin/bash

# Feature: Given a text file with one tree representation per line,
#          read trees from the file,
#          remove trace subtrees from them and
#          output resulting trees in the same format as in the input file.


  if diff <(./3_notrace wsj_0100.tree) acceptance_tests/pg02/3_notrace/wsj_0100.noTrace
  then
    echo "PASSED: $0"
    exit 0
  else
    echo "FAILED: $0"
    exit 1
  fi
