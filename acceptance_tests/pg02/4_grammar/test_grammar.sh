#!/usr/bin/bash

# Feature: Given a text file with one tree representation per line,
#          - read trees from the file
#          - output all the LTG productions of the form N -> N_1 ... N_k,
#            where N, N_1, ... , N_K are non-terminals, that are necessary
#            to generate the input trees contained in the file
#          - along with the number of times they occur in input trees
#
# ASSUME: - given trees *do not* contain any trace subtrees so that
#         - all leaf nodes are lexical items


  if diff <(./3_notrace wsj_0100.tree | ./4_grammar) acceptance_tests/pg02/4_grammar/grammar
  then
    echo "PASSED: $0"
    exit 0
  else
    echo "FAILED: $0"
    exit 1
  fi
