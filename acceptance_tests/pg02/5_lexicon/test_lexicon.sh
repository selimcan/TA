#!/usr/bin/bash

## Feature: Given a text file with one tree representation per line,
##          - read trees from the file
##          - output all the LTG productions of the form N -> q,
##            where q in Q is lexical material, that are necessary
##            to generate the input trees contained in the file
##          - along with the number of times N -> q rule is observed.
##
## ASSUME: - given trees do *not* contain any trace subtrees so that
##         - all leaf nodes are lexical material
##         - the input file does *not* contain subtrees like the following:
##                                        PRP
##                                       / | \
##                                     Mr. J. Taxpayer
##           i.e. non-terminal nodes have at most one lexical leaf


  if diff <(./3_notrace wsj_0100.tree | ./5_lexicon) acceptance_tests/pg02/5_lexicon/lexicon
  then
    echo "PASSED: $0"
    exit 0
  else
    echo "FAILED: $0"
    exit 1
  fi
