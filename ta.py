#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import copy

## Functions for:
## - reading a tree given its representation as a string
## - calculating its height
## - producing all LTG productions necessary to generate it
## - removing trace subtrees from a tree
## - producing productions where both sides are non-terminals (syntactic productions)
## - producing productions where right-hand side is a terminal (lexical productions)
##
## UNIT TESTS: py.test-3 pg01.py (requires python3-pytest package)
## ACCEPTANCE TESTS: bash acceptance_tests/run_all.sh


################ Data definitions


class Node:
    def __init__(self, label, children):
        self.label = label
        self.children = children

    def __eq__(self, other):
        if not other:
            return False
        else:
            return self.label == other.label and self.children == other.children

    def __repr__(self):
        return "Node('" + self.label + "', " + str(self.children) + ')'

## Node is Node(String, ListOfNode)
## interpretation: a node of a tree with a label and a list of its children
##                 (its subtrees). A tree is represented by its root node,
##                 hence a

## Tree is a Node.

## ListOfNode is of one of:
## - None
## - [Node, ListOfNode]

T_1 = Node("NP", None)  ## NP

T_2 = Node("x3", None)  ## note that no distinction is made
                        ## between SIGMA's symbols and Q's symbols

T_3 = Node("PRP", [Node("NP", None)])  ## PRP
                                       ##  |
                                       ##  NP

T_4 = Node("NN", [Node("PRP", [Node("NP", None)]),  ##   NN
                  Node("x3", None)])                ##  /  \
                                                    ## PRP x3
                                                    ##  |
                                                    ## NP

T_5 = Node("SQ", [Node("CC", [Node("But", None)]),  ## line 40 in wsj_0100.tree
                  Node("MD", [Node("can", None)]),
                  Node("NP-SBJ", [Node("NNP", [Node("Mr.", None)]),
                                  Node("NNP", [Node("Hahn", None)])]),
                  Node("VP", [Node("VB", [Node("carry", None)]),
                              Node("NP", [Node("PRP", [Node("it", None)])]),
                              Node("PRT", [Node("IN", [Node("off", None)])])]),
                  Node(".", [Node("?", None)])])

# def fn_for_node(n):
#     ... n.label  ## String
#     fn-for-lon(n.children)
#
# def fn_for_lon(lon):
#     if not lon:
#         ...
#     else:
#         for n in lon:
#             fn_for_node(n)


## Production is (String (tupleof String) String),
##     where the last string is either LEXICAL or SYNTACTIC (see below).
## interp. first string represents the left-hand side of a rule,
##         strings in the nested tuple -- its right-hand side.
##         LEXICAL means that all right-hand side symbols are terminals,
##         SYNTACTIC -- that at least some of them are non-terminals.

P_1 = ('S', ('NP', 'VP'), 'gram')  ## S -> NP VP


################ Constants


SYNTACTIC = 's'
LEXICAL = 'l'


################ Reading: tokenize, parse and read_tree


## Inspired by the following: http://norvig.com/lispy.html

## String -> Tree
def read_tree(line):
    """Given a line of a treebank file, return tree."""

    def construct_tree(expr):
        if isinstance(expr, str):
            return Node(expr, None)
        elif len(expr) == 1:
            return Node(expr[0], None)
        else:
            label = expr[0]
            children = [construct_tree(arg) for arg in expr[1:]]
            return Node(label, children)

    return construct_tree(parse(line))

def test_read_tree():
    assert read_tree('( (NP) )') == T_1
    assert read_tree('( (x3) )') == T_2
    assert read_tree('( (PRP (NP)) )') == T_3
    assert read_tree('( (NN (PRP (NP)) x3) )') == T_4
    assert read_tree('( (SQ (CC But) (MD can) (NP-SBJ (NNP Mr.) (NNP Hahn))'
                     '(VP (VB carry) (NP (PRP it)) (PRT (IN off))) (. ?)) )') == T_5


## String -> (nestedListOf String)
def parse(line):
    """Read a Scheme-like expression from a string."""

    tokens = tokenize(line)[1:-1]  # get rid of the surrounding parens

    def read_from_tokens(tokens):
        if len(tokens) == 0:
            raise SyntaxError('unexpected EOF while reading')
        token = tokens.pop(0)
        if '(' == token:
            L = []
            while tokens[0] != ')':
                L.append(read_from_tokens(tokens))
            tokens.pop(0)  # pop off ')'
            return L
        elif ')' == token:
            raise SyntaxError('unexpected )')
        else:
            return token

    return read_from_tokens(tokens)

def test_parse():
    assert parse('( (NP) )') == ['NP']
    assert parse('( (x3) )') == ['x3']
    assert parse('( (PRP (NP)) )') == ['PRP', ['NP']]
    assert parse('( (NN (PRP (NP)) x3) )') == ['NN', ['PRP', ['NP']], 'x3']
    assert parse('( (SQ (CC But) (MD can) (NP-SBJ (NNP Mr.) (NNP Hahn)) (VP'
                 '(VB carry) (NP (PRP it)) (PRT (IN off))) (. ?)) )') ==\
           ['SQ', ['CC', 'But'], ['MD', 'can'], ['NP-SBJ', ['NNP', 'Mr.'],
           ['NNP', 'Hahn']], ['VP', ['VB', 'carry'], ['NP', ['PRP', 'it']],
           ['PRT', ['IN', 'off']]], ['.', '?']]


## String -> (listof String)
def tokenize(line):
    """Convert a string of characters into a list of tokens."""
    return line.replace('(', ' ( ').replace(')', ' ) ').split()

def test_tokenize():
    assert tokenize('( (SQ (CC But) (MD can) (NP-SBJ (NNP Mr.) (NNP Hahn)) (VP'
                    '(VB carry) (NP (PRP it)) (PRT (IN off))) (. ?)) )') ==\
           ['(', '(', 'SQ', '(', 'CC', 'But', ')', '(', 'MD', 'can',
            ')', '(', 'NP-SBJ', '(', 'NNP', 'Mr.', ')', '(', 'NNP', 'Hahn',
            ')', ')', '(', 'VP', '(', 'VB', 'carry', ')', '(', 'NP', '(',
            'PRP', 'it', ')', ')', '(', 'PRT', '(', 'IN', 'off', ')', ')', ')',
            '(', '.', '?', ')', ')', ')']


################ Height


## Tree -> Integer
def height(tree):
    """Return the height of the tree."""
    node = tree
    if not node.children:
        return 0
    else:
        return max([1 + height(child) for child in node.children])

def test_height():
    assert height(T_1) == 0
    assert height(T_2) == 0
    assert height(T_3) == 1
    assert height(T_4) == 2
    assert height(T_5) == 4


################ Productions: productions_bft and productions_dft


## Tree -> (generator Production)
def productions_bft(tree):
    """Return all productions necessary to produce the given tree
    in the breadth-first manner.
    """
    queue = [tree]
    while queue:
        node = queue.pop(0)
        if node.children:
            if any(child.children for child in node.children):
                yield node.label, tuple([child.label for child in node.children]), SYNTACTIC
            else:
                yield node.label, tuple([child.label for child in node.children]), LEXICAL
            for child in node.children:
                queue.append(child)

def test_productions_bfs():
    assert list(productions_bft(T_1)) == []
    assert list(productions_bft(T_2)) == []
    assert list(productions_bft(T_3)) == [('PRP', ('NP',), LEXICAL)]
    assert list(productions_bft(T_4)) == [('NN', ('PRP', 'x3'), SYNTACTIC),
                                          ('PRP', ('NP',), LEXICAL)]
    assert list(productions_bft(T_5)) == [('SQ', ('CC', 'MD', 'NP-SBJ', 'VP', '.'), SYNTACTIC),
                                          ('CC', ('But',), LEXICAL),
                                          ('MD', ('can',), LEXICAL),
                                          ('NP-SBJ', ('NNP', 'NNP'), SYNTACTIC),
                                          ('VP', ('VB', 'NP', 'PRT'), SYNTACTIC),
                                          ('.', ('?',), LEXICAL),
                                          ('NNP', ('Mr.',), LEXICAL),
                                          ('NNP', ('Hahn',), LEXICAL),
                                          ('VB', ('carry',), LEXICAL),
                                          ('NP', ('PRP',), SYNTACTIC),
                                          ('PRT', ('IN',), SYNTACTIC),
                                          ('PRP', ('it',), LEXICAL),
                                          ('IN', ('off',), LEXICAL)]


## Tree -> (generator Production)
def productions_dft(tree):  # TODO: recursify
    """Return all productions necessary to produce the given tree
    in the depth-first pre-order manner.
    """
    stack = [tree]
    while stack:
        node = stack.pop()
        if node.children:
            if any(child.children for child in node.children):
                yield node.label, tuple([child.label for child in node.children]), SYNTACTIC
            else:
                yield node.label, tuple([child.label for child in node.children]), LEXICAL
            for child in reversed(node.children):
                stack.append(child)

def test_productions_dfs():
    assert list(productions_dft(T_1)) == []
    assert list(productions_dft(T_2)) == []
    assert list(productions_dft(T_3)) == [('PRP', ('NP',), LEXICAL)]
    assert list(productions_dft(T_4)) == [('NN', ('PRP', 'x3'), SYNTACTIC),
                                          ('PRP', ('NP',), LEXICAL)]
    assert list(productions_dft(T_5)) == [('SQ', ('CC', 'MD', 'NP-SBJ', 'VP', '.'), SYNTACTIC),
                                          ('CC', ('But',), LEXICAL),
                                          ('MD', ('can',), LEXICAL),
                                          ('NP-SBJ', ('NNP', 'NNP'), SYNTACTIC),
                                          ('NNP', ('Mr.',), LEXICAL),
                                          ('NNP', ('Hahn',), LEXICAL),
                                          ('VP', ('VB', 'NP', 'PRT'), SYNTACTIC),
                                          ('VB', ('carry',), LEXICAL),
                                          ('NP', ('PRP',), SYNTACTIC),
                                          ('PRP', ('it',), LEXICAL),
                                          ('PRT', ('IN',), SYNTACTIC),
                                          ('IN', ('off',), LEXICAL),
                                          ('.', ('?',), LEXICAL)]


################ Removing trace subtrees: is_trace, remove_trace_subtrees


## Tree -> Tree
def remove_trace_subtrees(tree):
    """Remove trace subtrees from a given tree."""
    new_tree = copy.deepcopy(tree)
    if is_trace(new_tree):
        return None
    else:
        queue = [new_tree]
        while queue:
            node = queue.pop(0)
            if node.children:
                for child in node.children[:]:
                    if is_trace(child):  # TODO: consider memoization
                        node.children.remove(child)
                    else:
                        queue.append(child)
    return new_tree

def test_remove_trace_subtrees():
    assert remove_trace_subtrees(T_1) == T_1
    assert remove_trace_subtrees(T_2) == T_2
    assert not remove_trace_subtrees(Node('*', None))
    assert not remove_trace_subtrees(Node('-NONE-', [Node('*U*', None)]))
    assert remove_trace_subtrees(Node('NML', [Node('-NONE-', [Node('*U*', None)]),
                                              Node('NP', [Node('NN', [Node('officer', None)])])])) ==\
           Node('NML', [Node('NP', [Node('NN', [Node('officer', None)])])])
    assert not remove_trace_subtrees(Node('NML', [Node('-NONE-', [Node('*U*', None)]),
                                                  Node('-NP-SBJ-1', [Node('-NONE-', [Node('*T*', None)])])]))
    # line 4 of wsj_0100.tree (and of wsj_0100.notrace for the right-hand side)
    assert remove_trace_subtrees(read_tree("( (S (NP-SBJ (NNP Nekoosa)) (VP (VBZ has) (VP (VBN given) (NP (DT the)"
                                           "(NN offer)) (NP (NP (DT a) (JJ public) (JJ cold) (NN shoulder)) (, ,) (NP"
                                           "(NP (DT a) (NN reaction)) (SBAR (WHNP-2 (-NONE- *0*)) (S (NP-SBJ (NNP Mr.)"
                                           "(NNP Hahn)) (VP (VBZ has) (RB n't) (VP (VBN faced) (NP-2 (-NONE- *T*))"
                                           "(PP-LOC (IN in) (NP (NP (PRP$ his) (CD 18) (JJR earlier) (NNS"
                                           "acquisitions)) (, ,) (SBAR (WHNP-1 (DT all) (WHPP (IN of) (WHNP (WDT"
                                           "which)))) (S (NP-SBJ-1 (-NONE- *T*)) (VP (VBD were) (VP (VBN negotiated)"
                                           "(NP-1 (-NONE- *)) (PP-LOC (IN behind) (NP (DT the)"
                                           "(NNS scenes))))))))))))))))) (. .)) )")) ==\
           read_tree("( (S (NP-SBJ (NNP Nekoosa)) (VP (VBZ has) (VP (VBN given) (NP (DT the)"
                     "(NN offer)) (NP (NP (DT a) (JJ public) (JJ cold) (NN shoulder)) (, ,) (NP"
                     "(NP (DT a) (NN reaction)) (SBAR (S (NP-SBJ (NNP Mr.)"
                     "(NNP Hahn)) (VP (VBZ has) (RB n't) (VP (VBN faced)"
                     "(PP-LOC (IN in) (NP (NP (PRP$ his) (CD 18) (JJR earlier) (NNS"
                     "acquisitions)) (, ,) (SBAR (WHNP-1 (DT all) (WHPP (IN of) (WHNP (WDT"
                     "which)))) (S (VP (VBD were) (VP (VBN negotiated)"
                     "(PP-LOC (IN behind) (NP (DT the) (NNS scenes))))))))))))))))) (. .)) )")

## Tree -> Boolean
def is_trace(tree):
    """Return True if the tree is a trace (sub)tree."""
    root = tree
    if not root.children:
        return root.label[0] == '*'
    else:
        return all([is_trace(child) for child in root.children])

def test_is_trace():
    assert is_trace(T_1) == False
    assert is_trace(T_2) == False
    assert is_trace(Node('*', None)) == True
    assert is_trace(Node('-NONE-', [Node('*U*', None)])) == True
    assert is_trace(Node('NML', [Node('-NONE-', [Node('*U*', None)]),
                                 Node('NP', [Node('NN', [Node('officer', None)])])])) == False
    assert is_trace(Node('NML', [Node('-NONE-', [Node('*U*', None)]),
                                 Node('-NP-SBJ-1', [Node('-NONE-', [Node('*T*', None)])])])) == True


################ Presenting


## Tree -> String
def tree2string(tree):
    """Represent the tree as a one-line string."""
    root = tree
    if not root.children:
        return root.label
    else:
        return '(' + root.label + ' ' + ' '.join(map(tree2string, root.children)) + ')'

def test_tree2string():
    assert tree2string(T_1) == 'NP'
    assert tree2string(T_2) == 'x3'
    assert tree2string(T_3) == '(PRP NP)'
    assert tree2string(T_5) == '(SQ (CC But) (MD can) (NP-SBJ (NNP Mr.) (NNP Hahn)) ' \
                               '(VP (VB carry) (NP (PRP it)) (PRT (IN off))) (. ?))'
